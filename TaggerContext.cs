using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace date_tagger.Model
{
    public class TaggerContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseNpgsql("Server=127.0.0.1;Port=5432;Database=postgres;Uid=postgres;Pwd=postadmin;");
        }
        public DbSet<Tag_Date> Tag_Dates { get; set; }
        public DbSet<Message> Messages {get;set;}
    }
}
