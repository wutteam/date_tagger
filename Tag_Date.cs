using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace date_tagger.Model
{
    public class Tag_Date
    {
        [Key]
        public int id{get;set;}
        public int id_message {get;set;}
        public DateTime date{get;set;}
    }
}