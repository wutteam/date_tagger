using System;
using System.ComponentModel.DataAnnotations;

namespace date_tagger.Model
{
    public class Message
    {
        [Key]
        public int id{get;set;}
        public string message_original{get;set;}
        public string message_clean {get;set;}
        public string message_title {get;set;}
        public DateTime message_from_date {get;set;}
        public DateTime? message_to_date {get;set;} 
        public string message_url{get;set;}
    }
}