using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using date_tagger.Model;
using System.Text.RegularExpressions;
using static System.DateTime;
using System.Globalization;
using System.Text;

namespace date_tagger
{
    public class DateTagger
    {
        private readonly TaggerContext _context = new TaggerContext();
        public string Tag_Dates()
        {
            try
            {
                _context.Tag_Dates.RemoveRange(_context.Tag_Dates);
                _context.SaveChanges();
                foreach (var message in _context.Messages)
                {
                    var dates = GetDistinctDatesFromMessage(message);
                    foreach (var dateTmp in dates)
                    {
                        _context.Tag_Dates.Add(new Tag_Date() { id_message = message.id, date = dateTmp });
                    }
                }

                _context.SaveChanges();
                return "Date tagging Success!";
            }
            catch (Exception)
            {
                return "Date tagging Failed!";
            }
        }

        public IEnumerable<DateTime> GetDistinctDatesFromMessage(Message message)
        {
            var findDates = new List<DateTime>();
            var monthRegEx =
                "stycz(eń|nia)|lut(y|ego)|mar(zec|ca)|kwie(cień|tnia)|maj(a)?|czerw(iec|ca)|lip(iec|ca)|sierp(ień|nia)|wrze(sień|śnia)|październik(a)?|listopad(a)?|grud(zień|nia)";
            var regex = new Regex(@"(([0-9]?[0-9]/)?[0-9]?[0-9]\.[0-9][0-9]( ?\.?br(\.| )|\.[0-9][0-9][0-9][0-9])|[0-9]?[0-9] (stycz(eń|nia)|lut(y|ego)|mar(zec|ca)|kwie(cień|tnia)|maj(a)?|czerw(iec|ca)|lip(iec|ca)|sierp(ień|nia)|wrze(sień|śnia)|październik(a)?|listopad(a)?|grud(zień|nia))( ?\.?br(\.| )| [0-9][0-9][0-9][0-9]))");
            var matches = regex.Matches(message.message_clean);
            foreach (var match in matches)
            {
                string matchString = match.ToString();
                if (matchString.EndsWith("br.") || matchString.EndsWith("br "))
                {
                    matchString = $"{Regex.Replace(matchString, " ?\\.?br(\\.| )", "")}.{message.message_from_date.Year}";
                }
                if (Regex.IsMatch(matchString, monthRegEx))
                {
                    matchString = ConvertNameDateStringToDateTime(matchString);
                }
                if (matchString.Contains("/"))
                {
                    var dates = matchString.Split("/");
                    var actualDateString = dates[dates.Length - 1];
                    var actualdate = ParseDate(actualDateString);
                    for (int i = 0; i < dates.Length - 1; i++)
                    {
                        DateTime date = new DateTime(actualdate.Year, actualdate.Month, int.Parse(dates[i]));
                        findDates.Add(date);
                    }
                    findDates.Add(actualdate);
                }
                else
                {
                    var date = ParseDate(matchString);
                    findDates.Add(date);
                }
            }

            return findDates.Distinct();
        }


        public DateTime ParseDate(string matchString)
        {
            if (matchString[1] == '.')
                matchString = matchString.Insert(0, "0");

            TryParseExact(matchString, "dd'.'MM'.'yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out var date);

            return date;
        }

        private string ConvertNameDateStringToDateTime(string matchString)
        {
            matchString = Regex.Replace(matchString, "(.| )?stycz(eń|nia)(.| )?", ".01.");
            matchString = Regex.Replace(matchString, "(.| )?lut(y|ego)(.| )?", ".02.");
            matchString = Regex.Replace(matchString, "(.| )?mar(zec|ca)(.| )?", ".03.");
            matchString = Regex.Replace(matchString, "(.| )?kwie(cień|tnia)(.| )?", ".04.");
            matchString = Regex.Replace(matchString, "(.| )?maj(a)?(.| )?", ".05.");
            matchString = Regex.Replace(matchString, "(.| )?czerw(iec|ca)(.| )?", ".06.");
            matchString = Regex.Replace(matchString, "(.| )?lip(iec|ca)(.| )?", ".07.");
            matchString = Regex.Replace(matchString, "(.| )?sierp(ień|nia)(.| )?", ".08.");
            matchString = Regex.Replace(matchString, "(.| )?wrze(sień|śnia)(.| )?", ".09.");
            matchString = Regex.Replace(matchString, "(.| )?październik(a)?(.| )?", ".10.");
            matchString = Regex.Replace(matchString, "(.| )?listopad(a)?(.| )?", ".11.");
            matchString = Regex.Replace(matchString, "(.| )?grud(zień|nia)(.| )?", ".12.");


            return matchString;
        }
    }
}