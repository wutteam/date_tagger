cd /D "%~dp0"
IF EXIST .\bin\Debug\netcoreapp2.0\date_tagger.dll (
dotnet .\bin\Debug\netcoreapp2.0\date_tagger.dll
) ELSE (
rmdir /S /Q .\bin
rmdir /S /Q .\obj
dotnet build 
dotnet .\bin\Debug\netcoreapp2.0\date_tagger.dll
)